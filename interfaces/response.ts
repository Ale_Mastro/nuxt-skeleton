
interface ApiResponse {
  data: any;
  status: number;
  statusText: string;
  request: any;
}

export default ApiResponse;