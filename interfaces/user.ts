interface User {
  address: {};
  avatar: string;
  credit_card: {};
  email: string;
  id: number;
  [key: string]: string | object | number
}

export default User;