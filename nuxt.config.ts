export default {
  build: {
    extend(config: any) {
      config.resolve.alias['@interfaces'] = '@/interfaces';
    },
    loaders: {
      scss: {
        implementation: require('sass')
      }
    }
  },
  buildModules: [
    '@nuxt/typescript-build',
    '@nuxtjs/sass'
  ],
  modules: [
    '@pinia/nuxt',
  ],
  pinia: {
    autoImports: [
      // automatically imports `defineStore`
      'defineStore', // import { defineStore } from 'pinia'
      ['defineStore', 'definePiniaStore'], // import { defineStore as definePiniaStore } from 'pinia',
      'storeToRefs',
    ],
  },
    // Configure the Sass loader
    sass: {
      implementation: require('sass'),
    }
};
