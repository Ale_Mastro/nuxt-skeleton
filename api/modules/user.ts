import instance from '../index';
import ApiResponse from 'interfaces/response';

export const UserApi = {
  getUsers(): Promise<ApiResponse> {
    return instance.get('/users')
  }
};
