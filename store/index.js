import { createPinia } from 'pinia'
import { useCounterStore } from './modules/counter.js'

export const store = createPinia()

store.use(useCounterStore)

export default useCounterStore;
